#!/usr/bin/env python
# encoding: utf-8

'''
Simple GUI control of LEED camera and controller for B07
'''
# Python version compatibility
from __future__ import print_function, unicode_literals
from __future__ import absolute_import, division, with_statement

# Import standard library modules
import logging
import os
import platform
import sys
import math
import time

from ctypes import *
import numpy as np
import scipy as sp

from PIL import ImageQt
import scipy.misc as misc

# Import Qt modules
import PyQt4
from PyQt4 import QtCore, QtGui, uic

try:
    os.system("pyrcc4 res.qrc -o res_rc.py")
    os.system("pyuic4 MainWindow.ui -o MainWindow.py")
except any as e:
    print(e.msg())

import res_rc  # note this requires compiled resource file res_rc.py
__QT_TYPE__ = 'PyQt4' 

# Define globals
__APP_AUTHOR__ = 'Liam Deacon'
__APP_COPYRIGHT__ = '\xa9' + '2013-2015 {0}'.format(__APP_AUTHOR__)
__APP_DESCRIPTION__ = ('LEED Control & Acquisition Module\n')
__APP_DISTRIBUTION__ = 'LEEDCam.LEEDCam'  # for PyPI 
__APP_EMAIL__ = 'liam.m.deacon@gmail.com'
__APP_LICENSE__ = 'MIT License'
__APP_NAME__ = 'LEEDCam'
__APP_VERSION__ = '0.1.0-dev'
__PYTHON__ = "{0}.{1}.{2}".format(sys.version_info.major,
                                  sys.version_info.minor,
                                  sys.version_info.micro, 
                                  sys.version_info.releaselevel)
__UPDATE_URL__ = ""

DEFAULT_LOCATION = os.path.join(os.path.expanduser('~'), 'LEEDCam')

# Platform specific setup
if platform.system() is 'Windows':
    from ctypes import windll
    # Tell Windows Python is merely hosting the application (taskbar icon fix)
    windll.shell32.SetCurrentProcessExplicitAppUserModelID(__APP_NAME__)


class OCI_LEED(object):
    ''' OCI LEED myUSB activeX class '''
    BC_MODES = {'F': 'Filament', 'W': 'Wehnelt', 'N': 'Off', None: 'Off'}
    LEED_MODES = {'A': 'AES', 'L': 'LEED', None: 'N/A'}
    PC_MODES = {'F': 'Full PC Control', 'X': 'Read-only', None: 'N/A'}
    MAX_VOLTAGES = {'LEED': {'filament': 3.2, 
                             'energy': 750., 
                             'screen': 5000.,
                             'grid': 320.,
                             'focus': 320.,
                             'wehnelt': 40.,
                             'bc_filament': 0.2,
                             'bc_wehnelt': 0.2,
                             },
                    'AES': {'filament': 3.2, 
                            'energy': 3200., 
                            'screen': 200.,
                            'grid': 0.,
                            'focus': 0.,
                            'wehnelt': 40.,
                            'bc_filament': 0.02,
                            'bc_wehnelt': 0.02,
                            }
                    }
    
    def __init__(self):
        ''' initialise USB object '''
        self.myUSB = self.getMyUSB()
        #self.clearComm()
    
    def getFilamentCurrent(self):
        try:
            return int(self._read('V7')) / 1000.
        except:
            return 0

    def getBeamEnergy(self):
        try:
            if self.mode.upper() == 'LEED':
                return float(self._read('VA')) / 10.
            else:
                return float(self._read('VA'))
        except:
            return 0.

    def getScreenVoltage(self):
        try:
            return int(self._read('VD')) / 1000.
        except:
            return 0
        
    def getGridVoltage(self):
        try:
            return float(self._read('VB'))
        except:
            return 0.
    
    def getFocusVoltage(self):
        try:
            return float(self._read('VE'))
        except:
            return 0.
    
    def getWehneltVoltage(self):
        try:
            return int(self._read('V9')) / 10.
        except:
            return 0.
    
    @property
    def emission(self):
        try:
            return int(self._read('V8'))
        except:
            return 0
        
    @property
    def control(self):
        try:
            return self.PC_MODES[self._read('VS').split(' ')[1]]
        except:
            return None
    
    def getBcConstant(self):
        try:
            return self.BC_MODES[self._read('VS').split(' ')[2]]
        except:
            return None
        
    @property
    def mode(self):
        try:
            return self.LEED_MODES[self._read('VS').split(' ')[3]]
        except:
            None
    
    def setFilamentCurrent(self, current):
        if (current * 1000. <= self.MAX_VOLTAGES[self.mode]['filament'] 
                and current >= 0.):
            self._write('V1 {:4i}'.format(current * 1000.))
    
    def setBeamEnergy(self, energy):
        if (energy >= 0. and energy <= self.MAX_VOLTAGES[self.mode]['energy']):
            self._write('V2 {:4i}'.format(energy))
    
    def setScreenVoltage(self, voltage):
        if voltage >= 0. and voltage <= self.MAX_VOLTAGES[self.mode]['screen']:
            self._write('V3 {:4i}'.format(voltage * 1000.))
    
    def setGridVoltage(self, voltage):
        if voltage >= 0. and voltage <= self.MAX_VOLTAGES[self.mode]['grid']:
            self._write('V4 {:4i}'.format(voltage))
    
    def setFocusVoltage(self, voltage):
        if (voltage > 0. 
                and voltage * 10. <= self.MAX_VOLTAGES[self.mode]['focus']):
            self._write('V5 {:4i}'.format(voltage * 10.))
    
    def setWehneltVoltage(self, voltage):
        if (voltage >= 0. 
                and voltage / 10. <= self.MAX_VOLTAGES[self.mode]['wehnelt']):
            self._write('V6 {:4i}'.format(voltage * 10.))

    def setBcConstant(self, bc_mode):
        for key in self.BC_MODES:
            if self.BC_MODES[key].lower() == bc_mode.lower():
                self._write('VS {}\r'.format(key.upper()))
    
    filament = property(getFilamentCurrent, setFilamentCurrent)
    energy = property(getBeamEnergy, setBeamEnergy)
    screen = property(getScreenVoltage, setScreenVoltage)
    grid = property(getGridVoltage, setGridVoltage)
    focus = property(getFocusVoltage, setFocusVoltage)
    wehnelt = property(getWehneltVoltage, setWehneltVoltage)
    bc_constant = property(getBcConstant, setBcConstant)
    
    def clearComm(self):
        try:
            return self.myUSB.ClearComm()
        except:
            sys.stderr.write("Failed to clear COM\n")
            sys.stderr.flush()
    
    def testUSB(self):
        ''' setup the USB link with OCI LEED controller '''
        self.myUSB.TestComm(0)
        
    def getMyUSB(self):
        ''' get a handle to activeX COM object '''
        try:
            import win32com.client
            return win32com.client.Dispatch("OCI.MyUSB.1")
        except ImportError as err:
            sys.stderr.write(err.message + '\n')
            sys.stderr.flush()
            return None
    
    def _read(self, cmd='VA'):
        try:
            if not self.myUSB.PortOpen:
                self.myUSB.CommPort = 0
                self.myUSB.PortOpen = True
            
            cmd = cmd.upper()
            cmd += '\r' if cmd[:-1] != '\r' else ''
            
            return "".join(self.myUSB.SendString(cmd, True).split(cmd[:2]))
        except any as err:
            sys.stderr.write('Failed to send command: "{}"'.format(cmd))
            sys.stderr.write(err.message + '\n')
            sys.stderr.flush()
            return 0
        
    def _write(self, cmd='VA'):
        try:
            if not self.myUSB.PortOpen:
                self.myUSB.CommPort = 0
                self.myUSB.PortOpen = True
            
            cmd = cmd.upper()
            cmd += '\r' if cmd[:-1] != '\r' else ''
            
            return self.myUSB.SendString(cmd, False)
        except any as err:
            sys.stderr.write('Failed to send command: "{}"'.format(cmd))
            sys.stderr.write(err.message + '\n')
            sys.stderr.flush()
            return None
    
    def _setBeamEnergy(self, energy):
        ''' set the beam energy '''
        # ensure port is open
        try:
            if not self.myUSB.PortOpen:
                self.myUSB.CommPort = 0
                self.myUSB.PortOpen = True
        
            # attempt to set the beam energy
            try:
                beamEnergy = float(energy)
                
            except ValueError:
                sys.stderr.write("Cannot convert energy input '%s' to float\n"
                                 % str(energy))
                sys.stderr.flush()
                return False
                
            # conditions on beam energy
            if beamEnergy >= 0.0 and beamEnergy <= 750:
                beamEnergy *= 10.0
                beamEnergy = math.ceil(beamEnergy)
    
                txString = "V2 %04i\r" % beamEnergy
                
                return self.myUSB.SendString(txString, False)
        except:
            sys.stderr.write("Failed to set LEED beam energy\n")
            sys.stderr.flush()

    def _getBeamEnergy(self):
        ''' read the beam energy '''
        
        # rxString = ''  # received string
        numString = ''  # number string
        s = ''  # position for the first blank
        nValue = float  # value
        done = False    # completed flag
        
        # ensure port is open
        try:
            if not self.myUSB.PortOpen:
                self.myUSB.CommPort = 0
                self.myUSB.PortOpen = True
        except AttributeError as err:
            sys.stderr.write(err.message + '\n')
            sys.stderr.flush()
            return None
            
        # send a string
        rxString = str(self.myUSB.SendString("VA\r", True))
        if rxString != "":
            s = rxString.rfind(" ")
            
            if s > 0:
                i = s + 1
                done = False
                
                while i < len(rxString) and not done:
                    if rxString[i] == '0':
                        done = True

                    if not done:
                        i += 1
                
                if done:
                    numString = rxString[i:]
                    try:  # to convert to number
                        nValue = int(numString) / 10.0
                        return nValue
                    
                    except ValueError:
                        sys.stderr.write("Cannot read beam energy"
                                         " - received '%s'\n" % numString)
                        sys.stderr.flush()
                        return None


class LeedCamera(object):
    features = {"ExposureTimeAbs": {'type': c_double, 
                                    'getter': 'VimbaDLL.featureFloatGet', 
                                    'setter': 'VimbaDLL.featureFloatSet',
                                    'handle': 'self.camera._handle',
                                    'min': 26., 'max': 60000000.},
                "MaxWidth": {'type': c_int64, 
                                    'getter': 'VimbaDLL.featureIntGet', 
                                    'setter': 'int',
                                    'handle': 'self.camera._handle',
                                    'min': 1, 'max': 10000000000000, },
                "MaxHeight": {'type': c_int64, 
                                    'getter': 'VimbaDLL.featureIntGet', 
                                    'setter': 'int',
                                    'handle': 'self.camera._handle',
                                    'min': 1, 'max': 10000000000000, }
                }
    
    def __init__(self, camera='Vimba', number=0, exposure=1):
        if not camera in ['Vimba']:
            raise ValueError("'%s' is not a valid camera type")
        
        self.camera_interface = camera
        if camera == 'Vimba':
            import pymba
            
            try:
                self.vimba = pymba.Vimba()
                self.vimba.startup()
                # discover any GigE cameras
                system = self.vimba.getSystem()
                if system.GeVTLIsPresent:
                    system.runFeatureCommand("GeVDiscoveryAllOnce")
                    time.sleep(0.2)
                else:
                    sys.stderr.write("GigE transport layer not found\n")
                    sys.stderr.flush()
                    
                cameraIds = self.vimba.getCameraIds()
               
                # get and open a camera
                self.camera = self.vimba.getCamera(cameraIds[number])
                self.camera.openCamera()
                self.camera.AcquisitionMode = 'SingleFrame'
                
                # create new frame for camera
                self.frame = self.camera.getFrame()
                self.buffer_frame = self.camera.getFrame()
                self.frame.announceFrame()
                
                self.liveFeedActive = False
                
            except pymba.VimbaException as ve:
                sys.stderr.write(ve.message + '\n')
                sys.stderr.flush()
            
            except IndexError:
                sys.stderr.write("No %s camera found\n" 
                                 % self.camera_interface)
                sys.stderr.flush()
                                
            except any as e:
                sys.stderr.write(e.message)
                sys.stderr.flush()
                
    def _getFeature(self, feature=''):
        from pymba.vimbadll import VimbaDLL
        
        if feature in self.features:
            var = self.features[feature]['type']()
            f = eval(self.features[feature]['getter'])
            hdl = eval(self.features[feature]['handle'])
            f(hdl, feature, byref(var))
            return var.value
        
        return None
    
    def _setFeature(self, feature, value):
        from pymba.vimbadll import VimbaDLL
        
        if feature in self.features:
            if value <= self.features[feature]['max'] and value >= self.features[feature]['min']:
                var = self.features[feature]['type'](value)
                f = eval(self.features[feature]['setter'])
                hdl = eval(self.features[feature]['handle'])
                f(hdl, feature, var)
                return var.value
    
    def getExposureTime(self):
        return self._getFeature("ExposureTimeAbs")
    
    def setExposureTime(self, time):
        self._setFeature("ExposureTimeAbs", time)
    
    exposure = property(getExposureTime, setExposureTime)
    
    @property
    def maxWidth(self):
        return self._getFeature("MaxWidth")
    
    @property
    def maxHeight(self):
        return self._getFeature("MaxHeight")
    
    @property
    def cameraIDs(self):
        return self.vimba.getCameraIds()
    
    @property
    def height(self):
        return self.frame.height
        
    @property
    def width(self):
        return self.frame.width
    
    def startLiveFeed(self):
        self.liveFeedActive = True
        # capture a camera image
        self.camera.AcquisitionMode = 'Continuous'
        self.camera.startCapture()
        self.frame.queueFrameCapture()
        self.camera.runFeatureCommand('AcquisitionStart')
                
    
    def stopLiveFeed(self):
        self.liveFeedActive = False
        self.camera.runFeatureCommand('AcquisitionStop')
        self.frame.waitFrameCapture()

        # clean up after capture
        self.camera.endCapture()
        self.camera.revokeAllFrames()
    
    @property
    def liveFeed(self):
        self.frame.waitFrameCapture(100)
        self.frame.queueFrameCapture()
        data = self.frame.getBufferByteData()
        data = np.ndarray(buffer=data,
                          dtype=np.uint8,
                          shape=(self.height, self.width))
        return data
        
    def takeImage(self):
        # do not take image if currently in continuous acquisition mode
        if self.liveFeedActive is True: 
            return
        
        if self.camera_interface == 'Vimba':
            try:
                # capture a camera image
                self.camera.AcquisitionMode = 'SingleFrame'
                
                self.camera.startCapture()
                self.frame.queueFrameCapture()
                self.camera.runFeatureCommand('AcquisitionStart')
                self.camera.runFeatureCommand('AcquisitionStop')
                self.frame.waitFrameCapture()
        
                # get image data...
                import numpy as np
                data = self.frame.getBufferByteData()
                data = np.ndarray(buffer=data,
                                  dtype=np.uint8,
                                  shape=(self.height, self.width, 1))
        
                # clean up after capture
                self.camera.endCapture()
                self.camera.revokeAllFrames()
                
                return data
                
            except any as e:
                sys.stderr.write(e.message + '\n')
                sys.stderr.flush()
                return None
            
    def __del__(self):
        if self.camera_interface == 'Vimba': 
            try:
                self.camera.closeCamera()
                self.vimba.shutdown()
                
            except any as e:
                sys.stderr.write(e.message + '\n')
                sys.stderr.flush()

class RampDialog(QtGui.QDialog):
    def __init__(self, parent=None):
        super(RampDialog, self).__init__(parent)
        
        self.ui = uic.loadUi("RampDialog.ui", self)
        
        self.ui.initialEnergy.valueChanged.connect(self.checkEnergy)
        self.ui.finalEnergy.valueChanged.connect(self.checkEnergy)
        self.ui.rampTime.valueChanged.connect(self.calculateRamp)
        self.ui.rampRate.valueChanged.connect(lambda: 
            self.ui.rampTime.setValue((self.ui.finalEnergy.value() - 
                                       self.ui.initialEnergy.value()) / 
                                      (self.ui.rampRate.value() / 3600.)) )
        self.ui.browse.clicked.connect(self.getDirectory)
        self.ui.directory.editingFinished.connect(self.expandVars)
        
        self.ui.formatCombo.currentIndexChanged.connect(self.setFormat)
        
        self.format = self.ui.formatCombo.currentText()
        
    def checkEnergy(self):
        if self.ui.initialEnergy.value() > self.ui.finalEnergy.value():
            e0 = self.ui.initialEnergy.value()
            self.ui.initialEnergy.setValue(self.ui.finalEnergy.value()) 
            self.ui.finalEnergy.setValue(e0)
        
    def calculateRamp(self):
        energy_range = self.ui.finalEnergy.value() - self.ui.initialEnergy.value()
        self.ui.rampRate.setValue(energy_range / self.ui.rampTime.value() * 3600.)

    def getDirectory(self):
        fd = QtGui.QFileDialog()
        
        filename = fd.getExistingDirectory(self, "Select Sequence Directory...")
        
        if filename:
            self.ui.directory.setText(filename)
            
    def setFormat(self):
        if self.ui.formatCombo.currentIndex() == self.ui.formatCombo.count() - 1:
            dialog = QtGui.QInputDialog(self)
            fmt, ok = dialog.getText(self, "Format specifier", 
                                     "Special variables are: \n\n"
                                     "${eV} -> current energy (eV)\n"
                                     "${exp} -> exposure time (s)\n"
                                     "${filament} -> filament current (A)\n"
                                     "${emission} -> emission current (uA)\n"
                                     "${beam} -> beam current (nA)\n"
                                     "${wehnelt} -> Wehnelt voltage (V)\n"
                                     "${focus} -> focus voltage (V)\n"
                                     "${grid} -> grid voltage (V)\n"
                                     "${screen} -> screen voltage (kV)\n"
                                     "${camera} -> camera name\n"
                                     "-----------------------------------------\n"
                                     "${date} -> current date\n"
                                     "${time} -> current time\n"
                                     "${username} -> current user\n"
                                     "${host} -> current hostname\n", 
                                     QtGui.QLineEdit.Normal) 
            if ok:
                self.format = fmt
        else:
            self.format = self.ui.formatCombo.currentText()
        
    def expandVars(self):
        self.sender().setText(os.path.expandvars(
                                os.path.expanduser(
                                    self.ui.directory.text())))


class MainWindow(QtGui.QMainWindow):
    '''Class for main application widget'''
    updateRateMilliSeconds = 100
    
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        
        # dynamically load ui
        uiFile = "MainWindow.ui"  # change to desired relative ui file path
        self.ui = uic.loadUi(uiFile, self) 
        self.setWindowIcon(self.ui.windowIcon())
        
        self.init()
        self.initUi()
        
        # show main window only after initialisation is complete
        self.ui.show()

    def init(self):
        '''Class to initialise logging and non-gui objects''' 

        ######################################
        # APP LOGGING
        ######################################
        
        # create logger with 'spam_application'
        self.logger = logging.getLogger(__APP_NAME__)
        self.logger.setLevel(logging.DEBUG)
        
        # create file handler which logs all messages
        fh = logging.FileHandler(os.path.join(os.environ['TEMP'], __APP_NAME__ 
                + str('.log')))  # temp directory is emptied on system reboot
        formatter = logging.Formatter(
                        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        fh.setLevel(logging.INFO)  # change to taste
        
        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.WARNING)
        ch.setFormatter(formatter)
        
        # add the handlers to the logger
        self.logger.addHandler(ch)
        self.logger.addHandler(fh)

        self.leed = OCI_LEED()
        
        self.camera = LeedCamera()
        
        self.ui.actionPrint.setEnabled(False)
        
        self.lastDirectory = os.path.expanduser('~')

    def initUi(self):
        '''Class to initialise the Qt Widget and setup slots and signals'''

        # Setup slots
        self.ui.actionLoadImage.triggered.connect(self.loadImage)
        self.ui.actionSaveImage.triggered.connect(self.saveImage)
        self.ui.actionPrint.triggered.connect(self.printImage)
        self.ui.actionQuit.triggered.connect(self.close)
        
        self.ui.actionConnectLEED.triggered.connect(self.connectLEED)
        self.ui.actionCaptureImage.triggered.connect(self.captureImage)
        self.ui.actionCaptureIVSequence.triggered.connect(
                                                    self.captureImageSequence)
        self.ui.actionTestLEED.triggered.connect(self.leed.testUSB)
        
        self.ui.actionConnectCamera.triggered.connect(self.connectCamera)
        self.ui.actionSetEnergy.triggered.connect(self.setEnergy)
        self.ui.actionLiveFeed.toggled.connect(self.startOrStopLiveFeed)
        
        self.ui.actionAbout.triggered.connect(self.about)
        self.ui.actionAboutQt.triggered.connect(self.aboutQt)
        self.ui.actionContact.triggered.connect(self.contactDeveloper)
        self.ui.actionHelp.triggered.connect(self.help)
        
        self.ui.captureImageButton.clicked.connect(self.captureImage)
        self.ui.saveImageButton.clicked.connect(self.saveImage)
        
        self.ui.energySlider.valueChanged.connect(lambda x: 
                self.ui.energy.setText(str('{:.1f}'.format(x)) + 'eV'))
        self.ui.energySlider.valueChanged.connect(lambda x:
                                                  self.leed.setBeamEnergy(x))
        self.ui.energy.clicked.connect(self.setEnergy)
        
        self.ui.pcCombo.currentIndexChanged.connect(self.updateMode)
        
        self.ui.aesMenu.setEnabled(False)
        
        self.leedTimer = QtCore.QTimer(self)
        self.leedTimer.timeout.connect(self.updateLEED)
        
        self.feedTimer = QtCore.QTimer(self)
        self.feedTimer.timeout.connect(self.updateFeed)
        
        def imageInfo(evnt):
            x, y = (evnt.pos().x(), evnt.pos().y())
            if x < 0 or y < 0:
                return 
            try:
                intensity = self.image_data[x][y] 
                saturation = (1 - (intensity / np.iinfo(np.uint8).max)) * 100
                self.ui.statusbar.showMessage("({},{}) = {} [{}% saturation]"
                                              "".format(x, y, intensity, 
                                                        int(saturation)))
                return evnt.accept()
            except IndexError:
                pass  # outside image boundaries
            
        self.ui.image.mouseMoveEvent = imageInfo 
            
        
        self.updateUi()
    
    def startOrStopLiveFeed(self, state):
        if not self.camera.liveFeedActive:
            self.camera.startLiveFeed()
            self.feedTimer.start(100)
        else:
            self.camera.stopLiveFeed()
            self.feedTimer.stop()
    
    def updateLEED(self):
        try:
            self.ui.filament.setValue(self.leed.filament)
            self.ui.energy.setText('{}eV'.format(self.leed.energy))
            self.ui.screen.setValue(self.leed.screen)
            self.ui.grid.setValue(self.leed.grid)
            self.ui.focus.setValue(self.leed.focus)
            self.ui.wehnelt.setValue(self.leed.wehnelt)
            self.ui.filament.setValue(self.leed.bc_constant)
        except ValueError as err:
            sys.stderr.write(err.message + '\n')
            sys.stderr.flush()
        except TypeError as err:
            # assume LEED  is not communicating
            self.updateUi(disconnect=True)
            self.leedTimer.stop()
            
    def updateFeed(self):
        if self.camera.liveFeedActive:
            feed = self.camera.liveFeed
            pix = QtGui.QPixmap.fromImage(ImageQt.ImageQt(misc.toimage(feed)))
            self.image_data = feed
            self.ui.image.setPixmap(pix)
    
    def updateMode(self, control, warn=True):
        if control and warn:
            reply = QtGui.QMessageBox.warning(self, "WARNING!",
                "This will allow full PC control of the LEED controller, "
                "including sensitive voltage and current parameters.\n\n"
                "CONTINUE ONLY IF YOU KNOW WHAT YOU ARE DOING!!\n\n"
                "Do you wish to proceed?",
                        QtGui.QMessageBox.Yes| QtGui.QMessageBox.No)
            if reply == QtGui.QMessageBox.No:
                self.ui.pcCombo.setCurrentIndex(0)
                return
        
        for item in ['bcCombo', 'focus', 'filament', 'emission', 'beam', 
                     'wehnelt', 'grid', 'screen', 'energy', 'energySlider']:
            eval("self.ui.{}.setEnabled(control)".format(item))
        
        if control:
            self.ui.filament.valueChanged.connect(self.leed.setFilamentCurrent)
            self.ui.focus.valueChanged.connect(self.leed.setFocusVoltage)
            self.ui.grid.valueChanged.connect(self.leed.setGridVoltage)
            self.ui.screen.valueChanged.connect(self.leed.setScreenVoltage)
            self.ui.wehnelt.valueChanged.connect(self.leed.setWehneltVoltage)
            try:
                self.ui.bcCombo.currentIndexChanged.connect(lambda x: 
                    self.leed.setBcConstant(self.leed.BC_MODES.keys()[x]))
            except:
                pass
        else:
            try:
                self.ui.filament.valueChanged.disconnect(self.leed.setFilamentCurrent)
                self.ui.focus.valueChanged.disconnect(self.leed.setFocusVoltage)
                self.ui.grid.valueChanged.disconnect(self.leed.setGridVoltage)
                self.ui.screen.valueChanged.disconnect(self.leed.setScreenVoltage)
                self.ui.wehnelt.valueChanged.disconnect(self.leed.setWehneltVoltage)
            except:
                pass
            try:
                self.ui.bcCombo.currentIndexChanged.disconnect(lambda x: 
                    self.leed.setBcConstant(self.leed.BC_MODES.keys()[x]))
            except:
                pass
        
    def updateUi(self, disconnect=False):
        # set values for LEED related items
        leed_connected = bool(self.leed.myUSB) and not disconnect
        
        for i in ['energy', 'energySlider', 'actionSetEnergy', 
                  'bcCombo', 'focus', 'filament', 'emission', 'beam', 
                  'wehnelt', 'grid', 'screen', 'pcCombo']:
            eval("self.ui.{}.setEnabled(leed_connected)".format(i))
        self.updateMode(self.ui.pcCombo.currentIndex(), warn=False)
        
        if not leed_connected:
            self.ui.energy.setText('Not Connected') 
            self.ui.actionConnectLEED.setIcon(QtGui.QIcon(':/link.svg'))
            self.ui.actionConnectLEED.setToolTip('Connect to LEED PSU')
            self.leedTimer.stop()
        else: 
            self.ui.actionConnectLEED.setIcon(QtGui.QIcon(':/stop.svg'))
            try:
                self.ui.energy.setText('{}eV'.format(self.leed.energy))
            except:
                self.ui.energy.setText('{}eV'.format(0))
            self.ui.actionConnectLEED.setToolTip('Disconnect from LEED PSU')
            #self.leedTimer.start(250)
        
        # set values for Camera related items
        camera_connected = True #bool(self.camera)
        
        self.ui.captureImageButton.setEnabled(camera_connected)
        self.ui.saveImageButton.setEnabled(camera_connected)
        self.ui.actionSaveImage.setEnabled(camera_connected)
        self.ui.actionCaptureImage.setEnabled(camera_connected)
        self.ui.actionCaptureIVSequence.setEnabled(camera_connected)
        self.ui.actionLiveFeed.setEnabled(camera_connected)
        
        if not camera_connected:
            self.ui.actionConnectCamera.setIcon(QtGui.QIcon(':/link.svg'))
            self.ui.actionConnectCamera.setToolTip('Connect to camera')
            if self.ui.actionLiveFeed is True:
                self.feedTimer.stop()
        else:
            self.ui.exposureSpin.setValue(self.camera.exposure)
            self.ui.exposureSpin.setMaximum(self.camera.features['ExposureTimeAbs']['max'])
            self.ui.exposureSpin.valueChanged.connect(self.camera.setExposureTime)
            self.ui.actionConnectCamera.setIcon(QtGui.QIcon(':/stop.svg'))
            self.ui.actionConnectCamera.setToolTip('Disconnect camera')

            
    def loadImage(self, path=None):
        fd = QtGui.QFileDialog()
        filename = fd.getOpenFileName(self, "Load Image...",
                                      directory=path or self.lastDirectory,
                                      filter='',
                                      selectedFilter=None)
        if filename:
            pixmap = QtGui.QPixmap()
            pixmap.load(filename)
            self.ui.image.setPixmap(pixmap)
            self.ui.actionPrint.setEnabled(True)
    
    def saveImage(self, path=None):
        if self.camera.liveFeedActive:
            self.feedTimer.stop()
        
        fd = QtGui.QFileDialog()
        filters = ["Images (*.png *.bmp *.jpg)", "All Files (*)"]
        
        filename = fd.getSaveFileName(self, "Save Image...",
                                      directory=path or self.lastDirectory, 
                                      filter=";;".join(filters), 
                                      selectedFilter=filters[0])
        
        if filename:
            self.ui.statusbar.showMessage("Saving '{}'...".format(filename))
            self.ui.image.pixmap().save(filename)
            self.ui.statusbar.clearMessage()
            
        if self.camera.liveFeedActive:
            self.feedTimer.start(self.updateRateMilliSeconds)
    
    def printImage(self):
        "Prints the current image"
        # Create the printer
        printerobject = QtGui.QPrinter(0)
        # Set the settings
        printdialog = QtGui.QPrintDialog(printerobject)
        if printdialog.exec_() == QtGui.QDialog.Accepted:
            # Print
            pixmapImage = QtGui.QPixmap.grabWidget(self.ui.image)
            painter = QtGui.QPainter(printerobject)
            painter.drawPixmap(0, 0, pixmapImage)
            del painter
    
    def connectCamera(self):
        
        self.updateUi()
    
    def connectLEED(self):
        if self.leed.myUSB is None:
            self.leed = OCI_LEED()
        
        if self.leed.myUSB is None:
            QtGui.QMessageBox.warning(self, "LEED Warning:", 
                                      "Cannot connect to OCI LEED COM port\n")
        
        self.updateUi()
    
    def captureImage(self):
        try:
            img_data = self.camera.takeImage()
            img_data.resize(img_data.shape[:2])
            pix = QtGui.QPixmap.fromImage(ImageQt.ImageQt(misc.toimage(img_data)))
            self.image_data = img_data
            self.ui.image.setPixmap(pix)
        except AttributeError:
            pass
        
    def captureImageSequence(self):
        dialog = RampDialog(self)
        
        res, ok = dialog.exec_()
        
        dwell = 1.0
        startEnergy = 20.
        stopEnergy = 300.
        
        energy = startEnergy
        
        if ok:
            if self.camera.liveFeedActive:
                self.camera.stopLiveFeed()
            
            while energy < stopEnergy:
            
    
    def setEnergy(self):
        inp = QtGui.QInputDialog(self)
        try:
            maximum = self.leed.MAX_VOLTAGES[self.leed.mode]['energy']
        except:
            maximum = 7500.
        inp.setDoubleValue(float(self.ui.energy.text()[:-2]))
        value, ok = inp.getDouble(self, "Set Energy (eV)", 
                      "Enter energy value in eV:", 
                      value=float(self.ui.energy.text()[:-2]), 
                      min=0, max=maximum, 
                      decimals=1)
        if ok:
            self.ui.energy.setText(str(value) + 'eV')
            self.ui.energySlider.setValue(int(eval(str(
                                        self.ui.energy.text()[:-2]))))
            self.leed.energy = value
            
    # Show about dialog
    def about(self):
        """Display 'About' dialog"""
        text = __APP_DESCRIPTION__
        text += '\n\nAuthor: {0} \nEmail: {1}'.format(__APP_AUTHOR__, 
                                                      __APP_EMAIL__)
        text += '\n\nApp version: {0}'.format(__APP_VERSION__)
        text += '\n{0}'.format(__APP_COPYRIGHT__)
        text += '\n' + __APP_LICENSE__
        text += '\n\nPython: {0}'.format(__PYTHON__)
        text += '\nGUI frontend: {0} {1}'.format(__QT_TYPE__, 
                                                 QtCore.QT_VERSION_STR)

        msg = QtGui.QMessageBox.about(self, self.ui.windowTitle(), text)
    
    # Display about dialog
    def aboutQt(self):
        """Display Qt dialog"""
        QtGui.QApplication.aboutQt()
    
    # Report bug / email devs
    def contactDeveloper(self):
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(
                str("mailto: {email}?subject={name} feedback&body=").format(
                            email=__APP_EMAIL__, name=__APP_NAME__)))
        
    def help(self):
        """Display help"""
        QtGui.QMessageBox.information(self, 'Help', 
                                      'Help is not currently available')
        self.logger.error('unable to create Help dialog')

    def captureIVSequence(self, first_energy=20., last_energy=300.,
                          time=3600., exposure=1., path=DEFAULT_LOCATION):
        pass

# boilerplate function - should be applicable to most applications
def main(argv=None):
    '''Entry point if executing as standalone''' 
    if argv is None:
        argv = sys.argv
        
    app = QtGui.QApplication(sys.argv)
    window = MainWindow()
    
    app_icon = QtGui.QIcon()
    for px in [16, 24, 32, 48, 256]:
        app_icon.addFile(':/camera.svg', QtCore.QSize(px, px))
    app.setWindowIcon(app_icon)
    
    return app.exec_()

# Execute main function if running as standalone module
if __name__ == '__main__':
    main()