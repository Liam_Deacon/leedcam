# README #

## INSTALL ##

### How do I get set up? ###

* Install Python with scipy, PyQt4 and PIL - note these all come with Python(x,y)
* Install AVT VIMBA SDK for camera driver 
* Install Pymba 
* Install OCI LEED software (you may need to copy the drivers onto the system $PATH)

### Contribution guidelines ###

LEEDCam is free software, released under the GPL 2.0+. As such suggestions, improvements & code contributions are all welcomed to the project. Here are a few suggestions if you wish to get involved: 

* Writing tests
* Code review
* Raise issues
* Wiki guides

### Who do I talk to? ###

* Liam Deacon - liam.m.deacon@gmail.com